﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ME3Launcher
{
    public partial class Dialog : Form
    {
        private String message;

        private Dialog(String message, bool update)
        {
            InitializeComponent();

            this.message = message;

            lblMessage.Text = this.message;

            if(update)
                checkForUpdates();
        }

        public static void ShowMessage(String message, bool update)
        {
            new Dialog(message, true);
        }

        private void checkForUpdates()
        {
            UpdateCheck check = new UpdateCheck();
            try
            {
                if (UpdateCheck.checkForUpdates(Program.version))
                {
                    if (MessageBox.Show("An update is available, would you like to update?", "MELauncher Update", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        check.DownloadUpdater();
                    }
                    else
                    {
                        Program.startME3Launcher();
                    }
                }
                else
                {
                    Program.startME3Launcher();
                }
            }
            catch
            {
                Program.startME3Launcher();
            }
        }
    }
}
