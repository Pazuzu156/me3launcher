﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ME3Launcher
{
    public partial class DownloaderWindow : Form
    {
        public DownloaderWindow()
        {
            InitializeComponent();
            if (Settings.AOT)
                this.TopMost = true;
        }

        public void update(double percent)
        {
            if(percent == 100)
            {
                lblProgress.Text = "Finished!";
            }
            else
            {
                pbProgress.Value = int.Parse(Math.Truncate(percent).ToString());
                lblProgress.Text = pbProgress.Value.ToString() + "%";
            }
        }
    }
}
