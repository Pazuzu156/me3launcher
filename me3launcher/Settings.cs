﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace ME3Launcher
{
    public class Settings
    {
        public static bool AOT { get; private set; }

        public static bool StartUp { get; private set; }

        public static bool Exists { get; private set; }

        public static String OriginLocation { get; private set; }

        public static String ME2Location { get; private set; }

        public static String ME3Location { get; private set; }

        public static String LauncherSettingsFile { get; private set; }

        public static String ChangelogFile { get; private set; }

        public static String CrashHandlerFile { get; private set; }

        public static Settings getInstance() { return new Settings(); }

        // For conversion of settings. Will be removed within 6 months. 
        String LauncherSettingsFileOld;

        private Settings()
        {
            LauncherSettingsFileOld = Application.StartupPath + @"\Settings\ME3LauncherSettings.xml";
            LauncherSettingsFile = Application.StartupPath + @"\Settings\MELauncherSettings.xml";
            ChangelogFile = Application.StartupPath + @"\Settings\me3l_changelog.txt";
            CrashHandlerFile = Application.StartupPath + @"\Settings\ME3LauncherCrashHandler.lock";

            if (File.Exists(LauncherSettingsFile))
                Exists = true;
            else
            {
                ConvertSettings();
            }
        }

        public void loadSettings()
        {
            if(File.Exists(LauncherSettingsFileOld))
            {
                ConvertSettings();
            }
            else
            {
                if(File.Exists(LauncherSettingsFile))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(LauncherSettingsFile);
                    XmlNodeList nodes = doc.GetElementsByTagName("Option");

                    foreach (XmlNode node in nodes)
                    {
                        XmlAttributeCollection attribs = node.Attributes;

                        StringDictionary nodeAttribList = new StringDictionary();

                        foreach (XmlAttribute attrib in attribs)
                        {
                            nodeAttribList.Add(attrib.Name, attrib.Value);
                        }

                        if (nodeAttribList["name"] == "OriginLocation")
                        {
                            OriginLocation = nodeAttribList["value"];
                        }

                        if(nodeAttribList["name"] == "ME2Location")
                        {
                            ME2Location = nodeAttribList["value"];
                        }

                        if (nodeAttribList["name"] == "ME3Location")
                        {
                            ME3Location = nodeAttribList["value"];
                        }

                        if (nodeAttribList["name"] == "TopLevel")
                        {
                            if (nodeAttribList["value"] == "true")
                                AOT = true;
                            else
                                AOT = false;
                        }

                        if (nodeAttribList["name"] == "StartUp")
                        {
                            if (nodeAttribList["value"] == "true")
                                StartUp = true;
                            else
                                StartUp = false;
                        }
                    }
                }
            }
        }

        private void ConvertSettings()
        {
            if(File.Exists(LauncherSettingsFileOld))
            {
                File.Move(LauncherSettingsFileOld, LauncherSettingsFile);
                Exists = true;
            }
        }

        public void saveSettings(String originLocation, String me2Location, String me3Location, bool aot, bool startup)
        {
            if(!Directory.Exists(Application.StartupPath + @"\Settings"))
                Directory.CreateDirectory(Application.StartupPath + @"\Settings");

            XmlWriterSettings settingsXML = new XmlWriterSettings();
            settingsXML.Indent = true;
            settingsXML.IndentChars = "\t";

            XmlWriter writer = XmlWriter.Create(LauncherSettingsFile, settingsXML);
            writer.WriteStartDocument();
            writer.WriteComment(" ME3Launcher Settings File. Edit to your leasure, but beware, editing could cause corruption! ");

            setSettings(originLocation, me2Location, me3Location, aot, startup);

            // Setup settings to be written
            String tl = (AOT) ? "true" : "false";
            String su = (StartUp) ? "true" : "false";

            writer.WriteStartElement("Settings");
            WriteXmlElement(writer, "OriginLocation", "string", OriginLocation);
            WriteXmlElement(writer, "ME2Location", "string", me2Location);
            WriteXmlElement(writer, "ME3Location", "string", me3Location);
            WriteXmlElement(writer, "TopLevel", "boolean", tl);
            WriteXmlElement(writer, "StartUp", "boolean", su);
            writer.WriteEndElement();
            writer.WriteEndDocument();

            writer.Flush();
            writer.Close();
        }

        private void WriteXmlElement(XmlWriter writer, String name, String type, String value)
        {
            writer.WriteStartElement("Option");
            writer.WriteAttributeString("name", name);
            writer.WriteAttributeString("type", type);
            writer.WriteAttributeString("value", value);
            writer.WriteEndElement();
        }

        // All static properties are only set privately, this allows the settings to be set,
        // but only when saving.
        private void setSettings(String originLocation, String me2Location, String me3Location, bool aot, bool startup)
        {
            OriginLocation = originLocation;
            ME3Location = me3Location;
            ME2Location = me2Location;
            AOT = aot;
            StartUp = startup;

            // For startup because it uses the registry
            RegistryKey startupKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (startup)
                if(startupKey.GetValue("ME3Launcher") != null) // if old key still exists, change to MELauncher to reflect new name
                {
                    startupKey.DeleteValue("ME3Launcher", false);
                    startupKey.SetValue("MELauncher", Application.ExecutablePath.ToString());
                }
            else
                startupKey.DeleteValue("MELauncher", false);
        }
    }
}
