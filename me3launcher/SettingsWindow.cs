﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32;
using System.Diagnostics;

namespace ME3Launcher
{
    public partial class SettingsWindow : Form
    {
        public SettingsWindow()
        {
            InitializeComponent();

            // Initialize window components from settings
            initWindow();

            // Add obsolete tooltip to Delete TexMod checkbox option
            ToolTip tt = new ToolTip();
            tt.SetToolTip(cbDelete, "This option is obsolete and will be removed in the next update.");
        }

        private void initWindow()
        {
            if (Settings.AOT)
                this.TopMost = true;
            else
                this.TopMost = false;

            if (Settings.StartUp)
                cbStartup.Checked = true;

            if (Settings.AOT)
                cbAOT.Checked = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            new AboutWindow().Show();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            save();
            if(MessageBox.Show("Launcher settings have been saved!", "Launcher Settings", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
            {
                Program.Refresh(); // Restart the program to apply the settings.
            }
        }

        private void save()
        {
            Settings s = Settings.getInstance();
            s.saveSettings(Settings.OriginLocation, Settings.ME2Location, Settings.ME3Location, cbAOT.Checked, cbStartup.Checked);
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            ProcessStartInfo psi = new ProcessStartInfo();
            psi.FileName = Environment.GetFolderPath(Environment.SpecialFolder.Windows) + @"\hh.exe";
            psi.Arguments = @"MELauncherHelp.chm";
            Process.Start(psi);
        }
    }
}
