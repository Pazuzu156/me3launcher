﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace ME3Launcher
{
    public static class Program
    {
        #region Debugging Check
        #if DEBUG
        public static bool DEBUGON
        {
            get { return true; }
        }
        #else
        public static bool DEBUGON
        {
            get { return false; }
        }
        #endif
        #endregion

        public static String version
        {
            get;
            private set;
        }

        public static MainWindow mainWindow
        {
            get;
            private set;
        }

        public static bool ShowCLOnBoot
        {
            get;
            private set;
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(String[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Program.version = "3.0";

            Process[] p = Process.GetProcessesByName("MELauncher");
            if(p.Length > 1)
            {
                Thread.Sleep(1000);
                if(p.Length > 1)
                {
                    MessageBox.Show("You can only have one instance of ME3Launcher opened at once!", "MELauncher", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    beginLaunch(args);
                }
            }
            else
            {
                beginLaunch(args);
            }
        }

        static void beginLaunch(String[] args)
        {
            if (args.Length > 0)
            {
                if (args[0] == "100")
                {
                    File.Delete(@"MELauncher_Updater.exe");
                    init(version, true);
                }
                else if (args[0] == "200")
                {
                    File.Delete(@"MELauncher_Updater.exe");
                }
                else if (args[0] == "300")
                {
                    /*
                     * Argument 300 is the uninstall cleanup the installer can't delete
                     */
                    Settings s = Settings.getInstance();
                    s.loadSettings(); // initial load to get locations for TexMod to delete

                    if (File.Exists(Settings.ME3Location + @"\Binaries\Win32\TexMod.exe"))
                        File.Delete(Settings.ME3Location + @"\Binaries\Win32\TexMod.exe");

                    if (File.Exists(Settings.ME2Location + @"\Binaries\TexMod.exe"))
                        File.Delete(Settings.ME2Location + @"\Binaries\TexMod.exe");

                    s.saveSettings("", "", "", false, false);
                }
            }
            else
            {
                init(version, false);
            }
        }

        static void init(String version, bool cl)
        {
            ShowCLOnBoot = cl;
            string url;
            if (DEBUGON)
                url = "http://cdn.kalebklein.com/me3/debug/version.txt";
            else
                url = "http://cdn.kalebklein.com/me3/2plus/version.txt";

            if(ShowCLOnBoot)
            {
                startME3Launcher(); // ShowCLOnBoot is set true, so the ChangeLog window will show | For update completed
            }
            else
            {
                if (new StatusCheck(url).IsConnected)
                    Dialog.ShowMessage("Checking for updates, please wait...", true);
                else
                    startME3Launcher();
            }
        }

        public static void startME3Launcher()
        {
            mainWindow = new MainWindow();

            Application.Run(mainWindow);
        }

        public static void Refresh()
        {
            Application.Restart();
        }
    }
}
