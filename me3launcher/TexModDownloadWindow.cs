﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace ME3Launcher
{
    public partial class TexModDownloadWindow : Form
    {
        private MainWindow mainWindow;
        private DownloaderWindow downloadWindow;

        private String melocation, originlocation;

        public TexModDownloadWindow(String loc1, String loc2)
        {
            InitializeComponent();
            mainWindow = new MainWindow();
            downloadWindow = new DownloaderWindow();
            if (Settings.AOT)
                this.TopMost = true;

            this.originlocation = loc1;
            this.melocation = loc2;
        }

        private void bOkay_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            String msgbxText = "Since you choose to install TexMod manually, simply download and extract TexMod, then click the \"OK\" button to open Explorer and paste TexMod into this directory.";
            if(MessageBox.Show(msgbxText, "Manually Install TexMod", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
            {
                Process.Start("http://www.fileplanet.com/205418/200000/fileinfo/Texmod-v0.9b");
                ProcessStartInfo info = new ProcessStartInfo();
                info.FileName = "explorer";
                info.Arguments = melocation;
                Process.Start(info);
            }
        }

        private void bDownload_Click(object sender, EventArgs e)
        {
            StatusCheck check = new StatusCheck();
            if(check.IsConnected)
            {
                WebClient client = new WebClient();
                client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChangedEventHandler);
                client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_AsyncCompletedEventHandler);

                client.DownloadFileAsync(new Uri("http://cdn.kalebklein.com/me3/TexMod.exe"), melocation + "TexMod.exe");
                downloadWindow.ShowDialog();
            }
            else
            {
                MainWindow.NoInternetConnectionDialog();
            }
        }

        private void client_AsyncCompletedEventHandler(object sender, AsyncCompletedEventArgs e)
        {
            downloadWindow.Close();
            if(MessageBox.Show("Download has finished!", "TexMod Download", MessageBoxButtons.OK, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.OK)
            {
                this.Close();
            }
        }

        private void client_DownloadProgressChangedEventHandler(object sender, DownloadProgressChangedEventArgs e)
        {
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            double percentage = bytesIn / totalBytes * 100;
            downloadWindow.update(percentage);
            downloadWindow.Refresh();
        }
    }
}
