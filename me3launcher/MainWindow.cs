﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using Microsoft.Win32;
using System.Xml;
using System.Threading.Tasks;

namespace ME3Launcher
{
    public partial class MainWindow : Form
    {
        #region Variables
        Settings settings;
        bool isTexModEnabled = false,
            hasOrigin = false,
            hasME2 = false,
            hasME3 = false,
            autoDetect = false;
        private volatile bool threadRunning = false;
        private Task listenTask;
        #endregion

        #region delegates
        private delegate void KillThreadDelegate();
        #endregion

        #region Constructor
        public MainWindow()
        {
            settings = Settings.getInstance();
            settings.loadSettings(); // Settings only needs loaded once. Upon load, everything is initialized

            InitializeComponent();

            if (Settings.AOT)
                this.TopMost = true;
            else
                this.TopMost = false;

            if (Settings.Exists)
            {
                tbOrigin.Text = Settings.OriginLocation;
                tbME2.Text = Settings.ME2Location;
                tbME3.Text = Settings.ME3Location;

                checkLocations();
            }

            // Always check for crash. It's always possible, but it will always run
            // In the event a crash occurs as when TexMod is enabled, crash handling
            // is running
            runCrashHandler();

            // handle form closing event
            this.FormClosing += MainWindow_FormClosing;

            // Make sure to show changelog on update
            if (Program.ShowCLOnBoot)
                new ChangelogWindow().ShowDialog();
        }
        #endregion

        #region Threaded Methods
        private void ListenForTexModLaunch(String launched)
        {
            threadRunning = true;
            listenTask = Task.Factory.StartNew(() => { listen(launched); stopListening(); });
        }

        private void listen(String launched)
        {
            String game = (launched == "me2") ? "ME2Game" : "MassEffect3";
            while(threadRunning)
            {
                Process[] p = Process.GetProcessesByName(game);
                if (p.Length >= 1)
                    threadRunning = false;
                System.Threading.Thread.Sleep(500); // Pause thread every half second. Keeps CPU usage low
            }

            return;
        }

        private void stopListening()
        {
            if (!this.InvokeRequired)
                disable();
            else
                this.Invoke(new KillThreadDelegate(disable));
        }
        #endregion

        #region Main Component Methods
        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isTexModEnabled)
            {
                // If user closes application. Warn them about it, and revert changes
                if (e.CloseReason == CloseReason.UserClosing) // || e.CloseReason == CloseReason.WindowsShutDown || e.CloseReason == CloseReason.TaskManagerClosing)
                {
                    if (File.Exists(Settings.ME3Location + @"\Binaries\Win32\me3l.backup"))
                    {
                        MessageBox.Show("Oops, you forgot to disable TexMod! No worries, I got it for ya! :)", "TexMod Still Enabled", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                        // Remember to kill the thread using killThread, so everything is reverted
                        threadRunning = false;
                        stopListening();
                    }
                    else if (File.Exists(Settings.ME2Location + @"\Binaries\me2l.backup"))
                    {
                        MessageBox.Show("Oops, you forgot to disable TexMod! No worries, I got it for ya! :)", "TexMod Still Enabled", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        // Remember to kill the thread using killThread, so everything is reverted
                        threadRunning = false;
                        stopListening();
                    }
                }
                // User shutting down or killing task. Make the best effort possible to revert changes
                // Crash handler will take care of this if this step isn't completed on time
                else
                {
                    if(e.CloseReason == CloseReason.WindowsShutDown || e.CloseReason == CloseReason.TaskManagerClosing)
                    {
                        threadRunning = false;
                        stopListening();
                    }
                }
            }
        }

        private void bBrowseOrigin_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbdm = new FolderBrowserDialog();
            if (fbdm.ShowDialog() == DialogResult.OK)
                tbOrigin.Text = fbdm.SelectedPath;
        }

        private void bBrowseME2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbdm = new FolderBrowserDialog();
            if (fbdm.ShowDialog() == DialogResult.OK)
                tbME2.Text = fbdm.SelectedPath;
        }


        private void bBrowseME3_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbdm = new FolderBrowserDialog();
            if (fbdm.ShowDialog() == DialogResult.OK)
                tbME3.Text = fbdm.SelectedPath;
        }

        private void bCheckLocations_Click(object sender, EventArgs e)
        {
            checkLocations();
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bTMEnable_Click(object sender, EventArgs e)
        {
            if (bTMEnable.Text == "Enable TexMod")
            {
                enable();
            }
            else if (bTMEnable.Text == "Disable TexMod")
            {
                // make sure to close listening thread if it's open
                if (listenTask != null && threadRunning == true)
                {
                    threadRunning = false;
                }
            }
        }

        private void bOrOpen_Click(object sender, EventArgs e)
        {
            //if (MessageBox.Show("Once Origin is opened, run Mass Effect 3, then immediately click the \"Disable TexMod\" button, then procede using TexMod.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
            String message = (rbME2.Checked)
                ? "Once Origin is opened, run Mass Effect 2, this ensures cloud sync still works. MELauncher knows when TexMod launches, so everything will be reverted correctly for you."
                : "Once Origin is opened, run Mass Effect 3, this ensures cloud sync still works. MELauncher knows when TexMod launches, so everything will be reverted correctly for you.";
            if(MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
            {
                Process.Start(Settings.OriginLocation + @"\Origin.exe");
            }
        }

        private void btnAutoDetect_Click(object sender, EventArgs e)
        {
            autoDetect = true;
            findLocations();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            new SettingsWindow().ShowDialog();
        }
        #endregion

        #region Event Called Methods
        private void findLocations()
        {
            String ol;
            String m3l;
            String m2l;

            bool isOriginFound = false;
            bool isME2Found = false;
            bool isME3Found = false;

            lblME3Check.Text = "Searching...";
            lblME2Check.Text = "Searching...";
            lblOriginCheck.Text = "Searching...";

            lblME3Check.ForeColor = Color.FromArgb(1, 87, 39, 0);
            lblME2Check.ForeColor = Color.FromArgb(1, 87, 39, 0);
            lblOriginCheck.ForeColor = Color.FromArgb(1, 87, 39, 0);

            try
            {
                ol = lookForOriginLocation();
                isOriginFound = true;
                tbOrigin.Text = ol;

                m3l = lookForME3Location();
                isME3Found = true;
                tbME3.Text = m3l;

                m2l = lookForME2Location();
                isME2Found = true;
                tbME2.Text = m2l;

            }
            catch (Exception)
            {
                if (!isOriginFound)
                {
                    MessageBox.Show("An Origin installation cannot be found! If you have it installed, search for it the manual way by clicking the Browse button beside the Origin Location text field");
                    lblOriginCheck.ForeColor = Color.FromArgb(1, 192, 0, 0);
                    lblOriginCheck.Text = "Not Found!";
                }

                if(!isME2Found)
                {
                    MessageBox.Show("A Mass Effect 2 installation cannot be found! If you have it installed, search for it the manual way by clicking the Browse button beside the Mass Effect 2 Location text field");
                    lblME2Check.ForeColor = Color.FromArgb(1, 192, 0, 0);
                    lblME2Check.Text = "Not Found!";
                }

                if (!isME3Found)
                {
                    MessageBox.Show("A Mass Effect 3 installation cannot be found! If you have it installed, search for it the manual way by clicking the Browse button beside the Mass Effect 3 Location text field");
                    lblME3Check.ForeColor = Color.FromArgb(1, 192, 0, 0);
                    lblME3Check.Text = "Not Found!";
                }
            }
            finally
            {
                checkLocations();
            }
        }

        private String lookForOriginLocation()
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\Origin");
            String originLocation = key.GetValue("InstallLocation").ToString();
            if (originLocation != null)
            {
                return originLocation;
            }
            return null;
        }

        private String lookForME2Location()
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\BioWare\Mass Effect 2");
            String me2Location = key.GetValue("Install Dir").ToString();
            if (me2Location != null)
            {
                return me2Location.TrimEnd('\\');
            }
            return null;
        }

        private String lookForME3Location()
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\BioWare\Mass Effect 3");
            String me3Location = key.GetValue("Install Dir").ToString();
            if (me3Location != null)
            {
                return me3Location.TrimEnd('\\');
            }
            return null;
        }

        private void checkLocations()
        {
            String oL = tbOrigin.Text;
            String m2L = tbME2.Text;
            String m3L = tbME3.Text;

            if(File.Exists(oL + @"\Origin.exe"))
            {
                lblOriginCheck.ForeColor = Color.FromArgb(1, 0, 150, 0);
                lblOriginCheck.Text = "Found!";
                hasOrigin = true;
                tbOrigin.Enabled = false;
                bBrowseOrigin.Enabled = false;
            }
            else
            {
                lblOriginCheck.ForeColor = Color.FromArgb(1, 192, 0, 0);
                lblOriginCheck.Text = "Not Found!";
                hasOrigin = false;
            }

            if (File.Exists(m2L + @"\Binaries\ME2Game.exe"))
            {
                lblME2Check.ForeColor = Color.FromArgb(1, 0, 150, 0);
                lblME2Check.Text = "Found!";
                hasME2 = true;
                tbME2.Enabled = false;
                bBrowseME2.Enabled = false;
            }
            else
            {
                lblME2Check.ForeColor = Color.FromArgb(1, 192, 0, 0);
                lblME2Check.Text = "Not Found!";
                hasME2 = false;
            }

            if (File.Exists(m3L + @"\Binaries\Win32\MassEffect3.exe"))
            {
                lblME3Check.ForeColor = Color.FromArgb(1, 0, 150, 0);
                lblME3Check.Text = "Found!";
                hasME3 = true;
                tbME3.Enabled = false;
                bBrowseME3.Enabled = false;
            }
            else
            {
                lblME3Check.ForeColor = Color.FromArgb(1, 192, 0, 0);
                lblME3Check.Text = "Not Found!";
                hasME3 = false;
            }

            if (hasOrigin && (hasME3 || hasME2))
            {
                panel1.Visible = true;
                gbGameSelect.Visible = true;
                Settings s = Settings.getInstance();
                s.saveSettings(oL, m2L, m3L, Settings.AOT, Settings.StartUp);

                String message = null;
                if(hasME2 && hasME3)
                    message = "Origin, Mass Effect 2, and Mass Effect 3 were all found, and their locations have been saved to MELauncher for future use.";
                else if (hasME3)
                    message = "Origin and Mass Effect 3 were both found, and their locations have been saved to MELauncher for future use.";
                else if (hasME2)
                    message = "Origin and Mass Effect 2 were both found, and their locations have been saved to MELauncher for future use.";

                if(autoDetect)
                    MessageBox.Show(message, "Auto Detect", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void runCrashHandler()
        {
            if(File.Exists(Settings.CrashHandlerFile))
            {
                if (MessageBox.Show("MELauncher wasn't properly closed with TexMod enabled, and this can cause some serious problems. Click OK to fix this issue.", "MELauncher Error") == DialogResult.OK)
                {
                    disable();
                    File.Delete(Settings.CrashHandlerFile);
                }
            }
        }

        private void StartUp(bool check)
        {
            RegistryKey startupKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (check)
                if (startupKey.GetValue("ME3Launcher") != null) // if old key still exists, change to MELauncher to reflect new name
                {
                    startupKey.DeleteValue("ME3Launcher", false);
                    startupKey.SetValue("MELauncher", Application.ExecutablePath.ToString());
                }
                else
                    startupKey.DeleteValue("MELauncher", false);
        }
        #endregion

        #region Enable / Disable Methods
        private void enable()
        {
            if(rbME3.Checked)
            {
                if (File.Exists(Settings.ME3Location + @"\Binaries\Win32\TexMod.exe"))
                {
                    File.Move(Settings.ME3Location + @"\Binaries\Win32\MassEffect3.exe", Settings.ME3Location + @"\Binaries\Win32\me3l.backup");
                    File.Move(Settings.ME3Location + @"\Binaries\Win32\TexMod.exe", Settings.ME3Location + @"\Binaries\Win32\MassEffect3.exe");

                    // Take care of buttons on screen
                    bTMEnable.Text = "Disable TexMod";
                    bOrOpen.Enabled = true;
                    bClose.Enabled = false;
                    btnSettings.Enabled = false;
                    bCheckLocations.Enabled = false;
                    btnAutoDetect.Enabled = false;
                    isTexModEnabled = true;
                    gbGameSelect.Enabled = false;

                    // create crash handler file
                    FileStream fs = File.Open(Settings.CrashHandlerFile, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    fs.Close();

                    // Always, always, always add MELauncher to startup in case crashes happen
                    if (!Settings.StartUp)
                        StartUp(true);

                    ListenForTexModLaunch("me3");
                }
                else
                {
                    new TexModDownloadWindow(Settings.OriginLocation, Settings.ME3Location + @"\Binaries\Win32\").ShowDialog();
                }
            }
            else if(rbME2.Checked)
            {
                if (File.Exists(Settings.ME2Location + @"\Binaries\TexMod.exe"))
                {
                    File.Move(Settings.ME2Location + @"\Binaries\ME2Game.exe", Settings.ME2Location + @"\Binaries\me2l.backup");
                    File.Move(Settings.ME2Location + @"\Binaries\TexMod.exe", Settings.ME2Location + @"\Binaries\ME2Game.exe");

                    // Take care of buttons on screen
                    bTMEnable.Text = "Disable TexMod";
                    bOrOpen.Enabled = true;
                    bClose.Enabled = false;
                    btnSettings.Enabled = false;
                    bCheckLocations.Enabled = false;
                    btnAutoDetect.Enabled = false;
                    isTexModEnabled = true;
                    gbGameSelect.Enabled = false;

                    // create crash handler file
                    FileStream fs = File.Open(Settings.CrashHandlerFile, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    fs.Close();

                    // Always, always, always add MELauncher to startup in case crashes happen
                    if (!Settings.StartUp)
                        StartUp(true);

                    ListenForTexModLaunch("me2");
                }
                else
                {
                    new TexModDownloadWindow(Settings.OriginLocation, Settings.ME2Location + @"\Binaries\").ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("You need to select a game to enable TexMod for before proceding", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void disable()
        {
            if(rbME3.Checked)
            {
                if (File.Exists(Settings.ME3Location + @"\Binaries\Win32\me3l.backup"))
                {
                    File.Move(Settings.ME3Location + @"\Binaries\Win32\MassEffect3.exe", Settings.ME3Location + @"\Binaries\Win32\TexMod.exe");
                    File.Move(Settings.ME3Location + @"\Binaries\Win32\me3l.backup", Settings.ME3Location + @"\Binaries\Win32\MassEffect3.exe");

                    // Take care of buttons on screen
                    bTMEnable.Text = "Enable TexMod";
                    bOrOpen.Enabled = false;
                    bClose.Enabled = true;
                    btnSettings.Enabled = true;
                    bCheckLocations.Enabled = true;
                    btnAutoDetect.Enabled = true;
                    isTexModEnabled = false;
                    gbGameSelect.Enabled = true;

                    // create crash handler file
                    File.Delete(Settings.CrashHandlerFile);

                    // Always, always, always add ME3Launcher to startup in case crashes happen
                    if (!Settings.StartUp)
                        StartUp(false);
                }
                else
                {
                    new TexModDownloadWindow(Settings.OriginLocation, Settings.ME3Location).ShowDialog();
                }
            }
            else if(rbME2.Checked)
            {
                if (File.Exists(Settings.ME2Location + @"\Binaries\me2l.backup"))
                {
                    File.Move(Settings.ME2Location + @"\Binaries\ME2Game.exe", Settings.ME2Location + @"\Binaries\TexMod.exe");
                    File.Move(Settings.ME2Location + @"\Binaries\me2l.backup", Settings.ME2Location + @"\Binaries\ME2Game.exe");

                    // Take care of buttons on screen
                    bTMEnable.Text = "Enable TexMod";
                    bOrOpen.Enabled = false;
                    bClose.Enabled = true;
                    btnSettings.Enabled = true;
                    bCheckLocations.Enabled = true;
                    btnAutoDetect.Enabled = true;
                    isTexModEnabled = false;
                    gbGameSelect.Enabled = true;

                    // create crash handler file
                    File.Delete(Settings.CrashHandlerFile);

                    // Always, always, always add ME3Launcher to startup in case crashes happen
                    if (!Settings.StartUp)
                        StartUp(false);
                }
                else
                {
                    new TexModDownloadWindow(Settings.OriginLocation, Settings.ME2Location).ShowDialog();
                }
            }
        }
        #endregion

        #region Static Methods
        public static void NoInternetConnectionDialog()
        {
            MessageBox.Show("There is no current internet connection. Please connect to the Internet to continue!", "MELauncher", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        #endregion

        #region Overridden Methods
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if(keyData == Keys.F1)
            {
                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = Environment.GetFolderPath(Environment.SpecialFolder.Windows) + @"\hh.exe";
                psi.Arguments = @"MELauncherHelp.chm";
                Process.Start(psi);
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
        #endregion
    }
}
