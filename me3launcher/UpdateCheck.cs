﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;

namespace ME3Launcher
{
    public class UpdateCheck
    {
        DownloaderWindow downloadWindow;

        public UpdateCheck()
        {
            downloadWindow = new DownloaderWindow();
        }

        public static Boolean checkForUpdates(String version)
        {
            var lv = Version.Parse(version);
            var ov = Version.Parse(UpdateCheck.GetUpdateInfo());

            UpdateCheck.CurrentLocalVersion = lv.ToString();

            if (ov > lv)
            {
                return true;
            }
            else
                return false;
        }

        public static String CurrentLocalVersion
        {
            get;
            private set;
        }

        public static String CurrentOnlineVersion
        {
            get;
            private set;
        }

        private static String GetUpdateInfo()
        {
            string url;
            if(Program.DEBUGON)
                url = "http://cdn.kalebklein.com/me3/debug/version.txt";
            else
                url = "http://cdn.kalebklein.com/me3/2plus/version.txt";

            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            byte[] buffer = Encoding.UTF8.GetBytes("");
            request.ContentType = "application/x-www-from-urlencode";
            request.ContentLength = buffer.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(buffer, 0, buffer.Length);
            dataStream.Close();
            WebResponse res = request.GetResponse();
            dataStream = res.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            String resp = reader.ReadLine();
            reader.Close();
            dataStream.Close();
            res.Close();

            return resp;
        }

        public void DownloadUpdater()
        {
            WebClient client = new WebClient();
            client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
            client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);

            string url;
            if (Program.DEBUGON)
                url = "http://cdn.kalebklein.com/me3/debug/MELauncher_Updater.exe";
            else
                url = "http://cdn.kalebklein.com/me3/2plus/MELauncher_Updater.exe";

            client.DownloadFileAsync(new Uri(url), "MELauncher_Updater.exe");
            downloadWindow.ShowDialog();
        }

        private void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            Process.Start(@"MELauncher_Updater.exe");
            downloadWindow.Close();
        }

        private void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            double percentage = bytesIn / totalBytes * 100;
            downloadWindow.update(percentage);
            downloadWindow.Refresh();
        }
    }
}
