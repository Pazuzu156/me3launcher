﻿namespace ME3Launcher
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.label1 = new System.Windows.Forms.Label();
            this.tbOrigin = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tbME3 = new System.Windows.Forms.TextBox();
            this.bBrowseOrigin = new System.Windows.Forms.Button();
            this.bBrowseME3 = new System.Windows.Forms.Button();
            this.lblOriginCheck = new System.Windows.Forms.Label();
            this.lblME3Check = new System.Windows.Forms.Label();
            this.bCheckLocations = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bTMEnable = new System.Windows.Forms.Button();
            this.bOrOpen = new System.Windows.Forms.Button();
            this.bClose = new System.Windows.Forms.Button();
            this.btnAutoDetect = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tbME2 = new System.Windows.Forms.TextBox();
            this.bBrowseME2 = new System.Windows.Forms.Button();
            this.lblME2Check = new System.Windows.Forms.Label();
            this.gbGameSelect = new System.Windows.Forms.GroupBox();
            this.rbME3 = new System.Windows.Forms.RadioButton();
            this.rbME2 = new System.Windows.Forms.RadioButton();
            this.panel1.SuspendLayout();
            this.gbGameSelect.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Origin Base Location";
            // 
            // tbOrigin
            // 
            this.tbOrigin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOrigin.Location = new System.Drawing.Point(12, 29);
            this.tbOrigin.Name = "tbOrigin";
            this.tbOrigin.Size = new System.Drawing.Size(334, 20);
            this.tbOrigin.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mass Effect 3 Base Location";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // tbME3
            // 
            this.tbME3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbME3.Location = new System.Drawing.Point(12, 107);
            this.tbME3.Name = "tbME3";
            this.tbME3.Size = new System.Drawing.Size(334, 20);
            this.tbME3.TabIndex = 3;
            // 
            // bBrowseOrigin
            // 
            this.bBrowseOrigin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bBrowseOrigin.Location = new System.Drawing.Point(352, 27);
            this.bBrowseOrigin.Name = "bBrowseOrigin";
            this.bBrowseOrigin.Size = new System.Drawing.Size(75, 23);
            this.bBrowseOrigin.TabIndex = 4;
            this.bBrowseOrigin.Text = "Browse..";
            this.bBrowseOrigin.UseVisualStyleBackColor = true;
            this.bBrowseOrigin.Click += new System.EventHandler(this.bBrowseOrigin_Click);
            // 
            // bBrowseME3
            // 
            this.bBrowseME3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bBrowseME3.Location = new System.Drawing.Point(352, 105);
            this.bBrowseME3.Name = "bBrowseME3";
            this.bBrowseME3.Size = new System.Drawing.Size(75, 23);
            this.bBrowseME3.TabIndex = 5;
            this.bBrowseME3.Text = "Browse..";
            this.bBrowseME3.UseVisualStyleBackColor = true;
            this.bBrowseME3.Click += new System.EventHandler(this.bBrowseME3_Click);
            // 
            // lblOriginCheck
            // 
            this.lblOriginCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOriginCheck.AutoSize = true;
            this.lblOriginCheck.ForeColor = System.Drawing.Color.Green;
            this.lblOriginCheck.Location = new System.Drawing.Point(433, 32);
            this.lblOriginCheck.Name = "lblOriginCheck";
            this.lblOriginCheck.Size = new System.Drawing.Size(0, 13);
            this.lblOriginCheck.TabIndex = 6;
            // 
            // lblME3Check
            // 
            this.lblME3Check.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblME3Check.AutoSize = true;
            this.lblME3Check.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblME3Check.Location = new System.Drawing.Point(433, 110);
            this.lblME3Check.Name = "lblME3Check";
            this.lblME3Check.Size = new System.Drawing.Size(0, 13);
            this.lblME3Check.TabIndex = 7;
            // 
            // bCheckLocations
            // 
            this.bCheckLocations.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bCheckLocations.Location = new System.Drawing.Point(12, 137);
            this.bCheckLocations.Name = "bCheckLocations";
            this.bCheckLocations.Size = new System.Drawing.Size(163, 23);
            this.bCheckLocations.TabIndex = 8;
            this.bCheckLocations.Text = "Check Locations";
            this.bCheckLocations.UseVisualStyleBackColor = true;
            this.bCheckLocations.Click += new System.EventHandler(this.bCheckLocations_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.bTMEnable);
            this.panel1.Controls.Add(this.bOrOpen);
            this.panel1.Location = new System.Drawing.Point(328, 161);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(168, 60);
            this.panel1.TabIndex = 9;
            this.panel1.Visible = false;
            // 
            // bTMEnable
            // 
            this.bTMEnable.Location = new System.Drawing.Point(3, 3);
            this.bTMEnable.Name = "bTMEnable";
            this.bTMEnable.Size = new System.Drawing.Size(163, 23);
            this.bTMEnable.TabIndex = 0;
            this.bTMEnable.Text = "Enable TexMod";
            this.bTMEnable.UseVisualStyleBackColor = true;
            this.bTMEnable.Click += new System.EventHandler(this.bTMEnable_Click);
            // 
            // bOrOpen
            // 
            this.bOrOpen.Enabled = false;
            this.bOrOpen.Location = new System.Drawing.Point(3, 32);
            this.bOrOpen.Name = "bOrOpen";
            this.bOrOpen.Size = new System.Drawing.Size(163, 23);
            this.bOrOpen.TabIndex = 1;
            this.bOrOpen.Text = "Launch Origin";
            this.bOrOpen.UseVisualStyleBackColor = true;
            this.bOrOpen.Click += new System.EventHandler(this.bOrOpen_Click);
            // 
            // bClose
            // 
            this.bClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bClose.Location = new System.Drawing.Point(12, 195);
            this.bClose.Name = "bClose";
            this.bClose.Size = new System.Drawing.Size(163, 23);
            this.bClose.TabIndex = 10;
            this.bClose.Text = "Close MELauncher";
            this.bClose.UseVisualStyleBackColor = true;
            this.bClose.Click += new System.EventHandler(this.bClose_Click);
            // 
            // btnAutoDetect
            // 
            this.btnAutoDetect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAutoDetect.Location = new System.Drawing.Point(12, 166);
            this.btnAutoDetect.Name = "btnAutoDetect";
            this.btnAutoDetect.Size = new System.Drawing.Size(163, 23);
            this.btnAutoDetect.TabIndex = 15;
            this.btnAutoDetect.Text = "Auto Detect Locations";
            this.btnAutoDetect.UseVisualStyleBackColor = true;
            this.btnAutoDetect.Click += new System.EventHandler(this.btnAutoDetect_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSettings.Location = new System.Drawing.Point(331, 135);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(163, 23);
            this.btnSettings.TabIndex = 17;
            this.btnSettings.Text = "Settings";
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Mass Effect 2 Base Location";
            // 
            // tbME2
            // 
            this.tbME2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbME2.Location = new System.Drawing.Point(12, 68);
            this.tbME2.Name = "tbME2";
            this.tbME2.Size = new System.Drawing.Size(334, 20);
            this.tbME2.TabIndex = 19;
            // 
            // bBrowseME2
            // 
            this.bBrowseME2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bBrowseME2.Location = new System.Drawing.Point(352, 66);
            this.bBrowseME2.Name = "bBrowseME2";
            this.bBrowseME2.Size = new System.Drawing.Size(75, 23);
            this.bBrowseME2.TabIndex = 20;
            this.bBrowseME2.Text = "Browse..";
            this.bBrowseME2.UseVisualStyleBackColor = true;
            this.bBrowseME2.Click += new System.EventHandler(this.bBrowseME2_Click);
            // 
            // lblME2Check
            // 
            this.lblME2Check.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblME2Check.AutoSize = true;
            this.lblME2Check.Location = new System.Drawing.Point(433, 71);
            this.lblME2Check.Name = "lblME2Check";
            this.lblME2Check.Size = new System.Drawing.Size(0, 13);
            this.lblME2Check.TabIndex = 21;
            // 
            // gbGameSelect
            // 
            this.gbGameSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gbGameSelect.Controls.Add(this.rbME3);
            this.gbGameSelect.Controls.Add(this.rbME2);
            this.gbGameSelect.Location = new System.Drawing.Point(181, 137);
            this.gbGameSelect.Name = "gbGameSelect";
            this.gbGameSelect.Size = new System.Drawing.Size(144, 81);
            this.gbGameSelect.TabIndex = 23;
            this.gbGameSelect.TabStop = false;
            this.gbGameSelect.Text = "Select Game";
            this.gbGameSelect.Visible = false;
            // 
            // rbME3
            // 
            this.rbME3.AutoSize = true;
            this.rbME3.Location = new System.Drawing.Point(6, 42);
            this.rbME3.Name = "rbME3";
            this.rbME3.Size = new System.Drawing.Size(90, 17);
            this.rbME3.TabIndex = 1;
            this.rbME3.TabStop = true;
            this.rbME3.Text = "Mass Effect 3";
            this.rbME3.UseVisualStyleBackColor = true;
            // 
            // rbME2
            // 
            this.rbME2.AutoSize = true;
            this.rbME2.Location = new System.Drawing.Point(6, 19);
            this.rbME2.Name = "rbME2";
            this.rbME2.Size = new System.Drawing.Size(90, 17);
            this.rbME2.TabIndex = 0;
            this.rbME2.TabStop = true;
            this.rbME2.Text = "Mass Effect 2";
            this.rbME2.UseVisualStyleBackColor = true;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(508, 229);
            this.Controls.Add(this.gbGameSelect);
            this.Controls.Add(this.lblME2Check);
            this.Controls.Add(this.bBrowseME2);
            this.Controls.Add(this.tbME2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.bClose);
            this.Controls.Add(this.btnAutoDetect);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bCheckLocations);
            this.Controls.Add(this.lblME3Check);
            this.Controls.Add(this.lblOriginCheck);
            this.Controls.Add(this.bBrowseME3);
            this.Controls.Add(this.bBrowseOrigin);
            this.Controls.Add(this.tbME3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbOrigin);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(524, 268);
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mass Effect Launcher (For Mods)";
            this.panel1.ResumeLayout(false);
            this.gbGameSelect.ResumeLayout(false);
            this.gbGameSelect.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbOrigin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox tbME3;
        private System.Windows.Forms.Button bBrowseOrigin;
        private System.Windows.Forms.Button bBrowseME3;
        private System.Windows.Forms.Label lblOriginCheck;
        private System.Windows.Forms.Label lblME3Check;
        private System.Windows.Forms.Button bCheckLocations;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bClose;
        private System.Windows.Forms.Button bOrOpen;
        private System.Windows.Forms.Button bTMEnable;
        private System.Windows.Forms.Button btnAutoDetect;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbME2;
        private System.Windows.Forms.Button bBrowseME2;
        private System.Windows.Forms.Label lblME2Check;
        private System.Windows.Forms.GroupBox gbGameSelect;
        private System.Windows.Forms.RadioButton rbME3;
        private System.Windows.Forms.RadioButton rbME2;
    }
}

