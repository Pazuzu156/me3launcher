﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace ME3Launcher
{
    public partial class AboutWindow : Form
    {
        public AboutWindow()
        {
            InitializeComponent();
            lblVersion.Text = String.Format("Version: {0}", UpdateCheck.CurrentLocalVersion);
            if (Settings.AOT)
                this.TopMost = true;
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.kalebklein.com");
        }

        private void bChangelog_Click(object sender, EventArgs e)
        {
            new ChangelogWindow().ShowDialog();
        }
    }
}
