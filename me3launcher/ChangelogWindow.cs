﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Threading.Tasks;

namespace ME3Launcher
{
    public partial class ChangelogWindow : Form
    {
        String changelogFile;

        public ChangelogWindow()
        {
            InitializeComponent();

            //changelogFile = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\me3l_changelog.txt";
            changelogFile = Settings.ChangelogFile;

            loadChangelog();

            if (Settings.AOT)
                this.TopMost = true;
        }

        private void bOkay_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void loadChangelog()
        {
            if (new StatusCheck("http://cdn.kalebklein.com/me3/2plus/changelog.txt").IsConnected)
                downloadTask = Task.Factory.StartNew(() => loadChangelogFromWeb("http://cdn.kalebklein.com/me3/2plus/changelog.txt"));
            else
                loadCachedChangelog();
        }

        Task downloadTask;
        private void loadChangelogFromWeb(string url)
        {
            threadSetText("Loading changelog, please wait...");

            try
            {
                WebRequest r = WebRequest.Create(url);
                WebResponse re = r.GetResponse();
                var reader = new StreamReader(re.GetResponseStream());
                {
                    //threadSetText(reader.ReadToEnd());
                    string line, resp = "";
                    while ((line = reader.ReadLine()) != null)
                    {
                        resp += line + "\r\n";
                    }
                    threadSetText(resp);
                    Stream file = File.Open(changelogFile, FileMode.OpenOrCreate, FileAccess.Write);
                    StreamWriter writer = new StreamWriter(file, Encoding.UTF8);
                    writer.Write(resp);
                    writer.Close();
                    file.Close();
                }
                reader.Close();
            }
            catch(Exception)
            {
                if (!this.InvokeRequired)
                    loadCachedChangelog();
                else
                    this.Invoke(new LoadCachedLogDelegate(loadCachedChangelog));
            }
        }

        private delegate void SetChangeLogDelegate(string text);
        private delegate void LoadCachedLogDelegate();
        private void setChangeLog(string text)
        {
            tbChangelog.Text = text;
        }

        private void threadSetText(string text)
        {
            if (!this.InvokeRequired)
                setChangeLog(text);
            else
                this.Invoke(new SetChangeLogDelegate(setChangeLog), text);
        }

        private void loadCachedChangelog()
        {
            if (File.Exists(changelogFile))
            {
                Stream file = File.Open(changelogFile, FileMode.Open, FileAccess.Read);
                StreamReader reader = new StreamReader(file);
                String line = "";
                while ((line = reader.ReadLine()) != null)
                {
                    tbChangelog.Text += line + "\r\n";
                }
                reader.Close();
                file.Close();
            }
            else
            {
                tbChangelog.Text = "There is no internet connection. Please connect to the Internet to check the changelog.";
            }
        }
    }
}
