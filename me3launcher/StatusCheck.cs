﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace ME3Launcher
{
    public partial class StatusCheck
    {
        /// <summary>
        /// Boolean to tell if there's an internet connection or not
        /// </summary>
        public bool IsConnected
        {
            get;
            private set;
        }

        /// <summary>
        /// URL of whatever you wish to test a connection to
        /// Private - Only accessible to this class
        /// </summary>
        private string URL;

        public StatusCheck()
        {
            this.URL = "";
            testConnection();
        }

        public StatusCheck(String url)
        {
            this.URL = url;
            testConnection();
        }

        private void testConnection()
        {
            string url = (this.URL.Length == 0) ? "http://www.google.com" : this.URL;
            HttpWebRequest hwrq = (HttpWebRequest)HttpWebRequest.Create(url);
            hwrq.Timeout = 1000;

            try
            {
                HttpWebResponse hwrp = (HttpWebResponse)hwrq.GetResponse();
                if (hwrp.StatusCode == HttpStatusCode.OK)
                    IsConnected = true;
            }
            catch
            {
                IsConnected = false;
            }
        }
    }
}
