@echo off
set cf=/S /Q
echo.
echo Cleaning...
echo.
rmdir %cf% Release
rmdir %cf% Debug
rmdir %cf% build
del MergeLog.txt
del CLog.txt
rmdir %cf% me3launcher\obj
rmdir %cf% me3launcher\bin
rmdir %cf% "MELauncher Updater"\obj
rmdir %cf% "MELauncher Updater"\bin
echo.
echo Completed
