#define AppName "MELauncher"
; #define Version "2.1"
#define Publisher "Kaleb Klein"
#define AppURL "http://www.kalebklein.com"
#define SetupName "mel_setup"

[Setup]
AppId={{C539E332-208B-4CE1-BCC0-FFAD963935CF}
AppName={#AppName}
AppVersion={#Version}
AppVerName={#AppName} {#Version}
AppPublisher={#Publisher}
AppPublisherURL={#AppURL}
AppSupportURL={#AppURL}
AppUpdatesURL={#AppURL}
DefaultDirName={pf}\{#AppName}
DefaultGroupName={#AppName}
OutputBaseFilename={#SetupName}-{#version}
OutputDir={#Build}
SetupIconFile=me3.ico
WizardSmallImageFile=me3.bmp
Compression=lzma
SolidCompression=yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Dirs]
Name: "{app}\Settings"

[Files]
Source: "{#Build}\{#AppName}.exe"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "README.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "mel_help\MELauncherHelp.chm"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#AppName}.exe.config"; DestDir: "{app}"; Flags: ignoreversion

[Icons]
Name: "{group}\{#AppName}"; Filename: "{app}\{#AppName}.exe"
; Name: "{group}\{cm:ProgramOnTheWeb,{#AppName}}"; Filename: "{#AppURL}"
Name: "{group}\MELauncherHelp.chm"; Filename: "{app}\MELauncherHelp.chm"
Name: "{group}\{cm:UninstallProgram,{#AppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#AppName}"; Filename: "{app}\{#AppName}.exe"; Tasks: desktopicon

[Run]
Filename: "{sys}\icacls.exe"; Parameters: """{app}"" /grant Users:F"; Flags: shellexec waituntilterminated; Description: "Folder Permissions"; StatusMsg: "Changing Directory Permissions"
Filename: "{sys}\icacls.exe"; Parameters: """{app}\Settings"" /grant Users:F"; Flags: shellexec waituntilterminated; Description: "Folder Permissions"; StatusMsg: "Changing Directory Permissions"
Filename: "{app}\{#AppName}"; Flags: nowait postinstall skipifsilent; Description: "{cm:LaunchProgram,{#StringChange(AppName, '&', '&&')}}"
Filename: "{sys}\notepad"; Parameters: """{app}\README.txt"""; Flags: nowait postinstall; Description: "View Readme File"
Filename: "{win}\hh.exe"; Parameters: """{app}\MELauncherHelp.chm"""; Flags: nowait postinstall; Description: "View MELauncher Help File"

[UninstallRun]
Filename: "{app}\MELauncher.exe"; Parameters: "300"

[UninstallDelete]
Type: files; Name: "{app}\Settings\ME3LauncherSettings.xml"
Type: files; Name: "{app}\Settings\MELauncherSettings.xml"
Type: files; Name: "{app}\Settings\me3l_changelog.txt"
Type: files; Name: "{app}\Settings\mel_changelog.txt"
Type: files; Name: "{app}\Settings\ME3LauncherCrashHandler.lock"
Type: files; Name: "{app}\Settings\MELauncherCrashHandler.lock"