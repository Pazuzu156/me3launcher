@echo off

title ME3Launcher Build Tool
for /f "delims=" %%i in (version.txt) do set version=%%i
set cwd=%~dp0
set home=%cwd%
set release=Release
set build=%cwd%build
set iss="%PROGRAMFILES(x86)%\Inno Setup 5\iscc.exe"
set lib="%WINDIR%\Microsoft.NET\Framework\v4.0.30319"
set msbuild="%lib%\MSBuild.exe"
set ilmerge="%PROGRAMFILES(x86)%\Microsoft\ILMerge\ILMerge.exe"
set commonflags=/p:Configuration=%release%;AllowUnsafeBlocks=true /p:CLSCompliant=False

:stage
del MergeLog.txt
del CLog.txt
rmdir /S /Q %build%
rmdir /S /Q %release%

:build
echo ----------------------------------------------------------------------
echo Building package...
%msbuild% "%home%\ME3Launcher.sln" %commonflags% /tv:4.0 /p:TargetFrameworkVersion=v4.0 /p:Platform="Any Cpu" /p:OutputPath="%build%" /fl /flp:LogFile=CLog.txt
if errorlevel 1 goto build-error
echo.
mkdir "%cwd%Release"
copy /y "%cwd%build\MELauncher.exe" "%cwd%Release\MELauncher.exe";
copy /y "%cwd%build\MELauncher_Updater.exe" "%cwd%Release\MELauncher_Updater.exe";
echo.
echo Creating installer..
%iss% %cwd%setup.iss /dVersion=%version% /dBuild=%release%
if errorlevel 1 goto build-error

:done
echo.
echo ---------------------------------------------------------------------
echo Compile finished....
cd %cwd%
goto exit

:build-error
echo failed to compile...

:exit
echo.
echo A log for compile and merge has been created
echo.
echo CLog.txt is the compiler log
echo MergeLog.txt is the merger log
echo.
pause
