MELauncher - Mass Effect Launcher (For Mods)
MELauncher (c) 2014 Kaleb Klein. All Rights Reserved.

MELauncher is a simple launch tool for use with TexMod. This tool allows you to easily apply
mods to Mass Effect 2 and Mass Effec 3 without the need for manual installation or messing 
things up. Also, without the need for messing with any code!

To use this tool, simply find the base locations for Origin, ME2, and/or ME3.

Once done, click "Check Locations" and BOOM, you can now use the tool. Another option which
makes the entire process easier is to use the "Auto Detect" feature. This will automatically
detect your game installations, and save the locations for you with only one click.

TexMod is required for this tool, but just click "Enable TexMod" and follow the directions for
installing TexMod onto your system. To remove TexMod and MELauncher, just run the uninstaller
to remove all traces of MELauncher from your system, including TexMod.


Changelog (Current Version)
===========================
2014-12-21 3.0 - New Version Release
	+ 3.0 Introduces support for ME2. Now you can mod both ME2 and ME3
	- Removed a lot of old code.
	+ Improved updater
	+ ME3Launcher will from now on be known as MELauncher
	+ Added new Help file that can be read four different ways:
		+ Open it upon install
		+ Under "Settings" Clicking "Help"
		+ Pressing F1 on the main window
		+ Opening it through the MELauncher Directory
	* Addressed issue with updater not updating. Tested and working, but could still be an 
		issue for slower connections
