﻿namespace MELauncher_Updater
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.lblDoing = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.bOpen = new System.Windows.Forms.Button();
            this.bOpenLater = new System.Windows.Forms.Button();
            this.bBegin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblDoing
            // 
            this.lblDoing.AutoSize = true;
            this.lblDoing.Location = new System.Drawing.Point(13, 13);
            this.lblDoing.Name = "lblDoing";
            this.lblDoing.Size = new System.Drawing.Size(147, 13);
            this.lblDoing.TabIndex = 0;
            this.lblDoing.Text = "Updating <FILENAME> 100%";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(12, 29);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(414, 23);
            this.progressBar1.TabIndex = 1;
            // 
            // bOpen
            // 
            this.bOpen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bOpen.Enabled = false;
            this.bOpen.Location = new System.Drawing.Point(12, 87);
            this.bOpen.Name = "bOpen";
            this.bOpen.Size = new System.Drawing.Size(414, 23);
            this.bOpen.TabIndex = 2;
            this.bOpen.Text = "Open MELauncher";
            this.bOpen.UseVisualStyleBackColor = true;
            this.bOpen.Click += new System.EventHandler(this.bOpen_Click);
            // 
            // bOpenLater
            // 
            this.bOpenLater.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bOpenLater.Enabled = false;
            this.bOpenLater.Location = new System.Drawing.Point(12, 116);
            this.bOpenLater.Name = "bOpenLater";
            this.bOpenLater.Size = new System.Drawing.Size(414, 23);
            this.bOpenLater.TabIndex = 3;
            this.bOpenLater.Text = "Open MELauncher Later";
            this.bOpenLater.UseVisualStyleBackColor = true;
            this.bOpenLater.Click += new System.EventHandler(this.bOpenLater_Click);
            // 
            // bBegin
            // 
            this.bBegin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bBegin.Location = new System.Drawing.Point(12, 58);
            this.bBegin.Name = "bBegin";
            this.bBegin.Size = new System.Drawing.Size(414, 23);
            this.bBegin.TabIndex = 4;
            this.bBegin.Text = "Begin Update";
            this.bBegin.UseVisualStyleBackColor = true;
            this.bBegin.Click += new System.EventHandler(this.bBegin_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 152);
            this.Controls.Add(this.bBegin);
            this.Controls.Add(this.bOpenLater);
            this.Controls.Add(this.bOpen);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.lblDoing);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(454, 191);
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MELauncher Updater";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDoing;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button bOpen;
        private System.Windows.Forms.Button bOpenLater;
        private System.Windows.Forms.Button bBegin;
    }
}

