﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Diagnostics;

namespace MELauncher_Updater
{
    public partial class MainWindow : Form
    {
        private List<String> updateList = new List<String>();
        private String fileDownloading;
        private int downloadCount = 0;

        public MainWindow()
        {
            InitializeComponent();

            lblDoing.Text = @"Awaiting update to begin";
        }

        private void BeginUpdate()
        {
            tryToKill("ME3Launcher");
            tryToKill("MELauncher");

            if(File.Exists(@"ME3Launcher.exe"))
                File.Move(@"ME3Launcher.exe", @"ME3Launcher.exe.old");
            else
                File.Move(@"MELauncher.exe", @"MELauncher.exe.old");

            GetUpdateList();
        }

        private void tryToKill(String appName)
        {
            try
            {
                Process[] procs = Process.GetProcessesByName(appName);
                if (procs.Length >= 1)
                {
                    Process p = procs[0];
                    p.Kill();
                }
            }
            catch(Exception)
            {
                MessageBox.Show("There was an error killing the application");
            }
        }

        private void GetUpdateList()
        {
            string url;
            if (Program.DEBUGON)
                url = "http://cdn.kalebklein.com/me3/debug/update_files.txt";
            else
                url = "http://cdn.kalebklein.com/me3/2plus/update_files.txt";
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            byte[] buffer = Encoding.UTF8.GetBytes("");
            request.ContentType = "application/x-www-from-urlencode";
            request.ContentLength = buffer.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(buffer, 0, buffer.Length);
            dataStream.Close();
            WebResponse res = request.GetResponse();
            dataStream = res.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            String line = "";

            while ((line = reader.ReadLine()) != null)
            {
                updateList.Add(line);
            }

            reader.Close();
            dataStream.Close();
            res.Close();

            DownloadNewFile();
        }

        private void DownloadNewFile()
        {
            foreach(String file in updateList)
            {
                WebClient client = new WebClient();
                client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
                client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);

                string url;
                if (Program.DEBUGON)
                    url = "http://cdn.kalebklein.com/me3/debug/" + Uri.EscapeDataString(file);
                else
                    url = "http://cdn.kalebklein.com/me3/2plus/" + Uri.EscapeDataString(file);
                downloadCount++;

                fileDownloading = file;
                client.DownloadFileAsync(new Uri(url), file);
            }
        }

        private void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if(downloadCount == updateList.Count)
            {
                lblDoing.Text = "Update Completed!";
                bOpen.Enabled = true;
                bOpenLater.Enabled = true;
                if(File.Exists("ME3Launcher.exe.old"))
                    File.Delete(@"ME3Launcher.exe.old");
                else
                    File.Delete(@"MELauncher.exe.old");
            }
        }

        private void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            double percentage = bytesIn / totalBytes * 100;

            progressBar1.Value = int.Parse(Math.Truncate(percentage).ToString());

            String p = progressBar1.Value.ToString();

            lblDoing.Text = "Updating " + fileDownloading + ": " + p + "%";
        }

        private void bOpen_Click(object sender, EventArgs e)
        {
            ProcessStartInfo info = new ProcessStartInfo();
            info.Arguments = "100";
            info.FileName = @"MELauncher.exe";
            Process.Start(info);
            this.Close();
        }

        private void bOpenLater_Click(object sender, EventArgs e)
        {
            ProcessStartInfo info = new ProcessStartInfo();
            info.Arguments = "200";
            info.FileName = @"MELauncher.exe";
            Process.Start(info);
            this.Close();
        }

        private void bBegin_Click(object sender, EventArgs e)
        {
            bBegin.Enabled = false;
            BeginUpdate();
        }
    }
}
