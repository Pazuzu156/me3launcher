﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace MELauncher_Updater
{
    static class Program
    {
        #region Debugging Check
        #if DEBUG
        public static bool DEBUGON
        {
            get { return true; }
        }
        #else
        public static bool DEBUGON
        {
            get { return false; }
        }
        #endif
        #endregion
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());
        }
    }
}
