# MELauncher | Open Source
Open source (defunct) version of MELauncher.  
Stopped at version 3.0

## Tools Needed
[Visual Studio 2013](https://www.visualstudio.com/en-us/news/vs2013-community-vs.aspx)  
[Inno Setup 5](http://www.jrsoftware.org/isdl.php)  
[ILMerge](http://www.microsoft.com/en-us/download/details.aspx?id=17630)  
[HTML Help Workshop](https://msdn.microsoft.com/en-us/library/windows/desktop/ms669985%28v=vs.85%29.aspx)

## Building
To compile MELauncher, check build.bat and make sure all environment variables match the locations of each of the tools required above, with the exception of MSBuild. That usually never changes locations.

Run the ``build.bat`` file, and allow it to compile. All binaries are placed into ``build`` directory, and release files are placed into the ``Release`` directory. Before running ``build.bat`` make sure when updating the version of the application, to update ``version.txt`` as well, so it compiles the installer with the current version.

You'll need to change the update server links, as ME3Launcher update server is defunct and no longer supported, so it will fail to work every time. If you choose to host your own update server, be sure to use the following structure:

```
changelog.txt <- Houses latest update
changelog_full.txt <- Full changelog
MELauncher.exe <- Updated binary
MELauncher_Updater.exe <- Updater
MELauncherHelp.chm <- Compiled help file
unins000.dat <- Uninstaller information
unins000.exe <- Uninstaller
update_files.txt <- Houses list of files needed in update
version.txt <- Houses application version of update
```

## Cleanup
To clean up binaries, run ``clean.bat``

## Open Source Requirements
90% of the code used in this application is no longer used in recent versions of MELauncher. In fact, it's using an entirely different technology altogether. With this code being defunct, and I'll most likely never touch it again, pull requests are not a requirement. Do with the code what you will, just reference back to this particular repository. Thank you :D

